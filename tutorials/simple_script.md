# Using a text editor to write a simple Python script
## Introduction
Most people who are new to coding will likely use/have used an Integrated
Development Environment (IDE) - a program where software development takes place.
A place where you can write code and with the click of a button, turn it into the
required product (e.g. a compiled binary). Examples of IDEs include: Microsoft
Visual Studio and Eclipse.

Using IDE's can be very convenient, and they can really benefit your productivity.
Especially as they tend to contain built-in tools like debuggers, spellcheckers
and autocompletion.

However, for our purposes IDEs are simply overkill.
They abstract a lot of the details regarding what is happening
'under the hood' so we recommend that for the remainder of this guide,
you use a simple text editor and the command line to write your code and manage
your files.


## What is a text editor
A text editor is simply a program that allows you to open, view and edit plain
text files. Unlike word processors (like MS word), text editors do *not* add
formatting to text.

They are lighter-weight options than IDE's and often come with features like
syntax highlighting etc. But essentially, all they do is allow you to write/edit text.

If you're writing a program, you'll still need a complier/need to use the command
line to compile/execute the script. If you wish to debug your code, you'll need to use an
external debugger/linter.

Most text editors can handle many different languages and the 'mode' in which your text
editor will operate in will be determined by the file type. For example, if you
have the file `hello.py`, your text editor will recognise that this is likely a
python script and provide syntax highlighting for Python code.


## What text editor should I use?
There are an abundance of text editors to choose from and the majority of them
are free. However, most of the popular Linux distributions (e.g. Debian, Ubuntu
and Fedora) use GNOME as their desktop environment which, in turn, comes with the
text editor [gedit](https://wiki.gnome.org/Apps/Gedit), which may just simply be
named *text editor* from the [GNOME shell](https://en.wikipedia.org/wiki/GNOME_Shell).

Other popular, open-source text editors include:

* [VScode](https://code.visualstudio.com/) - Open source and developed by Microsoft.
* [Atom](https://atom.io/) - modern and developed by GitHub.
* [Vim](https://www.vim.org/) - older but *very* widely used.
* [Emacs](https://www.gnu.org/software/emacs/) - Also older, but *very* widely used.


## Using the text editor to write a Python script
*Note that for the following walkthrough, we'll assume that you have the text*
*editor gedit installed, however, feel free to use a text editor of your choice.*

As previously mentioned, a Python script can usually be identified by its
*.py* file extension. To create and open a file (with a *.py* extension) from
the command line, simply execute:

    gedit hello.py &

Note that the ampersand (`&`) on the end is optional, this executes the command as
a background process, meaning that we can continue to use the command line while
gedit is open.

Now, within the `hello.py` script, write:

    print('Hello world!')

Then save and close the script.

The `hello.py` file is now a Python script ready for executing! Try:

    python3 hello.py

And you should see: `Hello world!` output to the terminal.


## Write your first Python script - Gigasecond
Now you're ready to write your first Python script, just follow the instructions below.

### Instructions
Write a script (`gigasecond.py`) that asks the user for their date of birth, then,
with this information calculates the time and date at which they have lived for
1 Gigasecond. If the user has already lived for more than one gigasecond,
congratulate them.

Optional extra: Also ask the user at what time they were born on their birthday
and provide a more accurate answer.

HINT: You'll likely need to use Python's `datetime` module. If you are unfamilar with
importing modules in Python, ask a fellow codething for an explanation.

Note that we should simply be able to execute `python3 gigasecond.py` in order to
use this script.
