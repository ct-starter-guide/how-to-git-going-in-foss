# Using GitLab

## Introduction
Please note that it is strongly advised that you complete the
[Using Git](using_git.md) section before starting this section.

This section aims to cover the following topics:

* How we can host version controlled projects using various web-based services.
* Gitlab and its basic use.

As covered in the [previous section](using_git.md), Git (and other version control
systems) manage the history of our code by storing snapshots of the project.
However, in order to allow developers to colloborate better, there exist web-based
services which allow us to host Git-managed repositories. To name a few of the
popular services where we can (freely) host our Git repositories: bitbucket,
Codebase, GitLab, GitHub and SourceForge. See the
[git.wiki](https://git.wiki.kernel.org/index.php/GitHosting) for more information.

## Hosting Git-controlled source code on a server
While web-based Git repository hosting services are great for hosting our code
(or even other types of files, such as Word documents, pictures, etc.), most of
them have added many of their own features. Typical features include:

* Git repository management
* An issue/bug tracker.
* Code reviews - e.g. the ability to create and view merge/pull requests with
changelogs.
* Access control.
* The ability to "fork" projects
* Individual user profiles (which show their activity/contributions).
* Web-based graphical interfaces which allow us to run Git commands which we would
normally invoke on the command line (for example, `merge` and `rebase`).
* Wiki support
* Continuous Integration (CI) pipelines support.

If you do not know what these features are, don't worry, learning what these features
are, and what they do, will come once you start using the tool.

## What is GitLab
GitLab is a web-based application which aims to allow users to carry out the entire
developer operations and development lifecycle, also known as the DevOps lifecycle.

The capabilities of GitLab are vast, and many of its features are well beyond the
scope of this guide. However, if you're curious, see
[GitLab's product page](https://about.gitlab.com/stages-devops-lifecycle/).

For the purposes of this guide, we need to understand that GitLab can be used to
host our code in what is known as a repository (as described in the
[Hosting code](../README.md###Hosting_code) section), track issues with the code
in GitLab's [issue tracker](https://docs.gitlab.com/ee/user/project/issues/),
show proposed changes to the 'master' branch of the codebase in what's known as
['Merge Requests'](https://docs.gitlab.com/ee/user/project/merge_requests/) and
enable the running of 'pipelines' to continually test our changes to the codebase
in what is known as [Continuous Integration (CI)](https://docs.gitlab.com/ee/ci/).

## Hosting the 'scratch' project in GitLab
Providing you followed the [previous section](using_git.md), you should now have
a local, git controlled project. You are now going to 'push' this to GitLab so that
it is hosted remotely, under your own GitLab namespace. To do this, follow these steps:

1. First sign into https://gitlab.com
2. Click the 'New Project' button
3. Now, we're going to create a 'blank' project. Give your project a name.
    - Your project name could be anything, for example, 'my first project'. You
    should notice here that the 'project slug' has been automatically completed for you.
    It is adivised that you do not change this, or at least keep it sensible.
    This is important because GitLab URLs have the format:
    https://gitlab.com/<your_namespace>/<project_slug>
4. Optionally give your project a description
5. For now, keep the 'Visibility Level' as private and leave the 'initialise
repository with a README' checkbox unchecked.
6. Create project

That's it, you now have a repository on GitLab that is ready to host your code.
Now, to 'push' your project to GitLab, follow the 'Git Global Setup' instructions
followed by the 'Existing folder' instructions on the webpage. You should note that
for our 'scratch' project, you will not have to run the commands `git init` (as
the directory is already managed by Git), `git add .` (as we've already staged
files for commiting) and `git commit -m "Initial commit"` (as we've already
committed our changes).

Once you've pushed your project, reload the webpage (https://gitlab.com/<your_namespace>/<your_project>)
and you should see something like this:

![alt text](_images/GitLab_my_first_project.png)

This is the 'front page' of the respository, from here, we can see general information
about the project, for example, the project name and description, the total number
of commits, the files and directories in the top level of the project and the latest
commit. It's important to note that if the project includes a README, this is also
displayed on this page.

Notice the sidebar on the left hand side of the page, from here, you can access
GitLab's features, many of which will be covered in the following sections.

## Cloning a project (Recap)
Git has a `clone` command which allows us to clone a remote repository into a
new directory on our system.

However, there are two ways in which we can 'clone' a project, via https and via ssh.
It is recommended that for your own projects, or project of which you are a developer,
you clone the project's via ssh, because this will allow you to be able to push to
the project later, without having to repeatedly enter your password.

To clone a project via ssh, you first need to create an ssh key pair and then add your
*public* key to GitLab.

To generate the ssh key pair, open your terminal and execute:

    $  ssh-keygen -o -t rsa -b 4096 -C "your_email@example.com"

This will then prompt you to input a file path to save your SSH keys to. To use the
default file paths (recommended), just press `Enter`. This should place they keys
in the `~/.ssh/` directory.

Next, go to your GitLab profile:  https://gitlab.com/profile, on the left sidebar,
hover over the symbol which looks like a key, this should show "SSH Keys", click this.

Then copy your ssh key *which ends in .pub** (try `gedit ~/.ssh/id_rsa.pub` and then
copy the text) into the 'key' box. Give this a sensible title and then add the key.

You are now ready to clone from and push to repos via ssh!

Now, from your terminal, change to your home directory (`cd`) and make another
directory called 'different-location' and then change into this. Now, go back to
your project in GitLab and click on the "Clone" button, and then copy the 'clone with SSH'
text. Then, from your terminal, try `git clone <SSH_TEXT>`.

Providing all has gone well, you have now cloned the project via ssh!

So, you've just cloned your project from a remote repository, now use git to check
out your project's remotes: `git remote -v`.

## Using GitLab's main features
As previously mentioned in this section, GitLab has many features which we can make use of,
and rather than listing them all here with a tutorial on how to use each one, we will learn
how to use each of GitLab's main features as we go when completing the upcoming projects.
